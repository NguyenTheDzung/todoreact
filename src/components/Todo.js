import React from 'react'
import Button from '@atlaskit/button';
import styled, {css} from 'styled-components';
import CheckIcon from '@atlaskit/icon/glyph/check';
import CrossIcon from '@atlaskit/icon/glyph/cross';

const ButtonStyled = styled(Button)`
    margin-top: 5px;
    text-align: left;
    &, 
    &:hover{
    ${p => p.isCompleted && css`
        text-decoration: line-through;
    `}
    }
    &:hover{
        .check-icon {
            display: inline-block;
        }
        
    }

    .check-icon {
        display: none;

        &:hover {
            background-color: white;
            border-radius: 3px;
        }
    }
    .close-icon:hover {
            background-color: white;
            border-radius: 3px;
    }
`;


export default function Todo({todo, onCheckClick, removeClick}) {
  return <ButtonStyled
  isCompleted={todo.isCompleted} shouldFitContainer
   iconBefore={!todo.isCompleted &&(
   <span className='check-icon'
   onClick={() => onCheckClick(todo.id)}
   ><CheckIcon primaryColor='aqua' /></span>)
   }
   iconAfter={(
    <span className='close-icon'
    onClick={() => removeClick(todo.id)}
    ><CrossIcon primaryColor='aqua' /></span>)}
   >{todo.name}</ButtonStyled>;
}

