import React from 'react'
import Button from '@atlaskit/button';
import Todo from './Todo';

export default function TodoList({ todoList, onCheckClick, removeClick}) {
  return (
    <>
     {
       todoList.map((todo) => (
       <Todo key={todo.id} todo={todo} onCheckClick={onCheckClick}
       removeClick={removeClick} />)
       )}
    </>
  )
}
