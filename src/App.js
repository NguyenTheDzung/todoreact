import { useCallback, useEffect, useState } from "react";
import TodoList from "./components/Todolist";
import Textfield from '@atlaskit/textfield';
import Button from '@atlaskit/button';
import { v4 } from 'uuid';


const TODO_APP_STORAGE_KEY = "TODO_APP";


function App() {
  const [todoList, setTodoList] = useState([]);
  const [textInput, setTextInput] = useState("");

  useEffect(() => {
    const storageTodoList = localStorage.getItem(TODO_APP_STORAGE_KEY);
    if (storageTodoList) {
      setTodoList(JSON.parse(storageTodoList));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(TODO_APP_STORAGE_KEY, JSON.stringify(todoList));
  }, [todoList]);

 

const onTextInputChange = useCallback((e) => {
  setTextInput(e.target.value);
}, []);

const onAddBtnClick = useCallback((e) => {
  setTodoList([ {id: v4(), name: textInput, isCompleted: false}, ...todoList]);
  setTextInput('');
}, [textInput, todoList])


const onCheckClick = useCallback((id) => {
  setTodoList(prevState => prevState.map(todo => todo.id === id ?
    {...todo, isCompleted:true} : todo))
}, []);

const removeClick = useCallback((id) => {
  const tmp = [...todoList];
  const index = tmp.findIndex(item => item.id === id);
  if (index > -1) {
      tmp.splice(index, 1);
      setTodoList(tmp);
  }
}, [todoList])

  return (
    <>
      <h1 id="title"> Todo Task List </h1>
      <Textfield name="add-todo" 
      placeholder="Input your task list!" 
      elemAfterInput={<Button 
      isDisabled={!textInput} appearance='primary' 
      onClick={onAddBtnClick}>Add Task</Button>}
      css={ {padding: "2px 4px 2px"}}
      value={textInput}
      onChange={onTextInputChange}
      ></Textfield>
      <TodoList todoList={todoList} onCheckClick={onCheckClick} removeClick={removeClick}/>
    </>
    )
}

export default App;
